package order

import "gitlab.com/antoxa2614/rewiew/proto"

func NewOrder(OrderId int32, ProductName string, Quantity int32, TotalPrice float32, Client int32) *proto.Order {
	return &proto.Order{
		OrderId:     OrderId,
		ProductName: ProductName,
		Quantity:    Quantity,
		TotalPrice:  TotalPrice,
		Client:      Client,
	}
}
