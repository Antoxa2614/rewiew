package client

import (
	"context"
	"fmt"
	"github.com/rabbitmq/amqp091-go"
	protos "gitlab.com/antoxa2614/rewiew/proto"
	"google.golang.org/protobuf/proto"
	"log"
)

type Client struct {
	ch          *amqp091.Channel
	orderQueue  *amqp091.Queue
	orderStatus *amqp091.Queue
}

func NewClient(login, password, host, port string) *Client {
	conn, err := amqp091.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s", login, password, host, port))
	if err != nil {
		log.Fatal("RabbitMQ connection error", err)
	}
	//defer conn.Close()
	ch, err := conn.Channel()
	if err != nil {
		log.Fatal("channel RabbitMQ error", err)
	}
	//defer ch.Close()
	// Очередь заказов от клиента
	oq, err := ch.QueueDeclare(
		"order_queue", // name
		false,         // durable
		false,         // delete when unused
		false,         // exclusive
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		log.Fatal("Queue RabbitMQ error", err)
	}
	// Очередь статуса заказов
	os, err := ch.QueueDeclare(
		"order_status_queue", // name
		false,                // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		log.Fatal("Queue RabbitMQ error", err)
	}

	return &Client{
		ch:          ch,
		orderQueue:  &oq,
		orderStatus: &os,
	}

}
func (p *Client) SendOrder(ctx context.Context, order *protos.Order) error {
	orderBytes, err := proto.Marshal(order)
	if err != nil {
		log.Fatalf("Failed to marshal order: %v", err)
	}
	err = p.ch.PublishWithContext(ctx,
		"",
		p.orderQueue.Name,
		false,
		false,
		amqp091.Publishing{

			ContentType: "content/type",
			Body:        orderBytes,
		})
	return err
}

func (p *Client) GetStatus() (<-chan amqp091.Delivery, error) {
	msg, err := p.ch.Consume(
		p.orderStatus.Name, // queue
		"",                 // server
		true,               // auto-ack
		false,              // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)
	return msg, err
}
