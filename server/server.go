package server

import (
	"context"
	"fmt"
	"github.com/rabbitmq/amqp091-go"
	"log"
)

type Server struct {
	ch          *amqp091.Channel
	orderQueue  *amqp091.Queue
	orderStatus *amqp091.Queue
}

func NewServer(login, password, host, port string) *Server {
	conn, err := amqp091.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", login, password, host, port))
	if err != nil {
		log.Fatal("RabbitMQ connection error", err)
	}
	//defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal("RabbitMQ create channel error", err)
	}
	//defer ch.Close()
	// Очередь заказов от клиента
	q, err := ch.QueueDeclare(
		"order_queue", // name
		false,         // durable
		false,         // delete when unused
		false,         // exclusive
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		log.Fatal("RabbitMQ make queue error", err)
	}
	// Очередь статуса заказов
	os, err := ch.QueueDeclare(
		"order_status_queue", // name
		false,                // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		log.Fatal("Queue RabbitMQ error", err)
	}
	return &Server{ch: ch,
		orderQueue:  &q,
		orderStatus: &os}
}
func (p *Server) GetOrder() (<-chan amqp091.Delivery, error) {
	msg, err := p.ch.Consume(
		p.orderQueue.Name, // queue
		"",                // server
		true,              // auto-ack
		false,             // exclusive
		false,             // no-local
		false,             // no-wait
		nil,               // args
	)
	return msg, err
}
func (p *Server) SendStatus(ctx context.Context, message []byte) error {
	err := p.ch.PublishWithContext(ctx,
		"",
		p.orderStatus.Name,
		false,
		false,
		amqp091.Publishing{

			ContentType: "content/type",
			Body:        message,
		})
	return err
}
