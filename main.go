package main

import (
	"context"
	"github.com/joho/godotenv"
	"gitlab.com/antoxa2614/rewiew/client"
	order2 "gitlab.com/antoxa2614/rewiew/order"
	protos "gitlab.com/antoxa2614/rewiew/proto"
	"gitlab.com/antoxa2614/rewiew/server"
	"google.golang.org/protobuf/proto"
	"log"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error loading .env file")
	}
	login := os.Getenv("RABBITMQ_LOGIN")
	password := os.Getenv("RABBITMQ_PASSWORD")
	host := os.Getenv("RABBITMQ_HOST")
	port := os.Getenv("RABBITMQ_PORT")

	client := client.NewClient(login, password, host, port)
	server := server.NewServer(login, password, host, port)

	order := order2.NewOrder(2, "Panasonic", 2, 300, 21321)

	msgServer, err := server.GetOrder()
	if err != nil {
		log.Fatal("Getting Order data error")
		return
	}
	msgClient, err := client.GetStatus()
	if err != nil {
		log.Fatal("Getting Status data error")
		return
	}

	forever := make(chan bool)
	go func() {
		for d := range msgClient {
			status := &protos.OrderStatus{}
			if err := proto.Unmarshal(d.Body, status); err != nil {
				log.Printf("Failed to unmarshal status: %v", err)
				continue
			}

			log.Printf("Статус заказа у клиента: %+v", status)
		}
	}()
	go func() {
		for d := range msgServer {
			order := &protos.Order{}
			err := proto.Unmarshal(d.Body, order)
			if err != nil {
				log.Printf("Failed to unmarshal order: %v", err)
			}

			log.Printf("Заказ проверяется на складе: %+v", order)
			//Здесь должна быть логика проверки по базе данных
			status := &protos.OrderStatus{
				OrderId: order.OrderId,
				Success: true,
				Message: "Order processed successfully",
			}
			statusBytes, err := proto.Marshal(status)
			if err != nil {
				log.Printf("Failed to marshal status: %v", err)
			}
			err = server.SendStatus(context.Background(), statusBytes)
			if err != nil {
				log.Printf("Failed to publish status: %v", err)
			}
		}
	}()
	err = client.SendOrder(context.Background(), order)
	if err != nil {
		log.Fatal("Error send order")
	}
	log.Printf("Waiting for orders")
	<-forever

}
