module gitlab.com/antoxa2614/rewiew

go 1.20

require (
	github.com/joho/godotenv v1.5.1
	github.com/rabbitmq/amqp091-go v1.8.1
)

require google.golang.org/protobuf v1.31.0 // indirect
